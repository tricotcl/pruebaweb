<?php 
session_start();
ini_set("zlib.output_compression", "On");
include ('include/funciones.php');
include ('clases/clases_mensaje_sucursal.php');
include ('clases/clases_perfil.php');


/*
Prueba de Cambio Desde Tree
*/
$PC = new PCD_cons_0127();
$arrMsjSUC = $PC->get_PCD_cons_0127(); 
$mensajeSucursalSOS = "";
foreach($arrMsjSUC as $smsSuc){
    if($smsSuc["l_estado"]==1){
        if($mensajeSucursalSOS==""){
            $mensajeSucursalSOS = "(F5) ";
        }
        $mensajeSucursalSOS.= trim($smsSuc["l_nombre"])."|";
    }
}
$_SESSION['mensajeSucursalSOS'] = $mensajeSucursalSOS;

$suc			= obtiene_codigo_sucursal();
//echo $suc;
$busqueda  = '<div class="container_3 alpha " >';
//$busqueda .= 	'<div class="grid_8 " >';
$busqueda .= 		'<div class="grid_1-5 alpha omega">Paterno</div>';
$busqueda .= 		'<div class="grid_3 alpha omega"><input type="text" id="paterno"/></div>';
$busqueda .= 		'<div class="grid_1-5 alpha omega">Materno</div>';
$busqueda .= 		'<div class="grid_3 alpha omega"><input type="text" id="materno"/></div>';
$busqueda .= 		'<div class="clear"></div>';
$busqueda .= 		'<br/>';
$busqueda .= 		'<div class="grid_8 center"><input type="button" name="buscar" value="BUSCAR" onclick="busquedaApe(1)"/></div>';
$busqueda .= 		'<br/><br/>';
$busqueda .= 	'<div class="clear"></div>';
//$busqueda .= 	'<div class=" grid_8 ">';

//$busqueda .= 	'</div>';
$busqueda .= 	'<div class=" grid_8 " id="busqueda"></div>';
$busqueda .= '</div>';
#echo "<pre>";print_r($_SESSION);

if(isset($_SESSION['datosEjecutivo'])){
    $datosEjecutivo = $_SESSION['datosEjecutivo'];
}else{
    $datosEjecutivo = "";
}

$perfil = $_SESSION['datosEjecutivo']['id_tipo_cuenta'];
// Comentario de Prueba 2

$PUIC= new PCD_trae_perfil_menu();
$PUIC->set_l_perfil($perfil);
$opcMenu = $PUIC->get_PCD_trae_perfil_menu();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('include/includes.php');  ?>
<script src="tq-js/jstq-consulta-datos.js"></script>
<script>
    var ejecutivoCredito = '<?php echo ($datosEjecutivo['rut']=="")?"NO":"SI"; ?>' ;
    var	fn1='<div class="numerico2">$</div><div class="numerico8">';
    var	fn2='</div>';
    var sms = [];
    var agrupador = 0;
    var docFin = 0;
    function clasifi(){
        var data =  $('input[name=number]');
        $.ajax({
            url: "ajax/ajax_clasificacion.php",
            type: "POST",
            data: data,
            dataType: "json",
            async:true,
            cache: false,
            beforeSend: function(){
                agrupador++;
            },
            success: function(html) {
                $('#clasificacion').html(html.html);
                if(html.html==""){
                    $('#clasificacion').html('<div class="cajainput">SIN INFORMACION</div>');
                }
            },
            complete: function(){
                docFin++;
            }
        });
    }
    function firma(){
        if(ejecutivoCredito=="SI"){
            $.ajax({
                url: "ajax/ajax_firma_contrato.php",
                type: "POST",
                dataType: "json",
                async:true,
                cache: false,
                complete: function(){
                    docFin++;
                },
                beforeSend: function(){
                    agrupador++;
                },
                success: function(html) {
                    if(html == 1){
                        sms.push('EL CLIENTE NO HA FIRMADO DOCUMENTOS,DESEA FIRMAR DOCUMENTO? <button onclick="location.href=\'firma.php\'">SI</button>');					
                    }
                    
                }
            });	
        }
    }	
    function mensajes(number){
        //alert(1);
        var data = 'rut=' + number;
        if(ejecutivoCredito=="SI"){
            $.ajax({
                url: "ajax/ajax_mensajes.php",
                type: "POST",
                dataType: "json",
                data: data,
                async:true,
                cache: false,
                complete: function(){
                    docFin++;
                },
                beforeSend: function(){
                    agrupador++;
                },
                success: function(html) {
                    if(html.avance !="0"){
                        if(html.avance=="CLIENTE CON DISPONIBLE PARA AVANCE"){
                            sms.push("CLIENTE CON DISPONIBLE PARA AVANCE");
                        }else{
                            sms.push('DISPONIBLE AVANCE $ ' + html.avance);
                        }					
                    }
                    if(html.multi !="0"){
                        sms.push('DISPONIBLE MULTICOMERCIO $ ' + html.multi + ". SUJETO A CONSULTA DE INFORMES COMERCIALES");					
                    }
                    if(html.descto !="0"){
                        //sms.push('CLIENTE CON PROMOCION DE $ 10.000 DE DESCUENTO POR COMPRAS SUPERIORES A $ 20.000 IMPRIMIR CUPON DE DESCUENTO ');					
                        sms.push('CLIENTE CON PROMOCION DE 20% A CLIENTES LEALES, FAVOR VALIDAR SMS');					
                    }
                    if(html.sobre !="0"){
                        sms.push('REPACTACION -- OFERTA PIE $ ' + html.sobre);					
                    }
                    if(html.oferta !="0"){
                        sms.push('EL VALOR DEL PIE $ ' + html.oferta);					
                    }					
                    if(html.traspaso =="1"){
                        sms.push('<b>CLIENTE SUJETO A SER TRASPASADO A VISA</b>');					
                    }
                    if(html.descuento_traspaso =="0"){
                        sms.push('<b>CLIENTE CON PROMOCION TRASPASO 25% EN PRIMERA COMPRA EN TRICOT Y 5% EN TRICOT CONNECT VALIDO SOLO EL DIA DEL TRASPASO</b>');					
                    }
                    if(html.devo!=""){
                        sms.push(html.devo);					
                    }			
                }
            });
        }
    }
    function reloj(){
        var fecha = new Date;
        var horas = fecha.getHours();
        horas=(horas<10)?"0"+horas:horas;
        var minutos = fecha.getMinutes();
        minutos=(minutos<10)?"0"+minutos:minutos;
        var segundos = fecha.getSeconds();
        segundos=(segundos<10)?"0"+segundos:segundos;
        $("#reloj").html(horas+":"+minutos+":"+segundos);
    }
        // Prueba Numero 3 *****
   $(document).ready(function(e) {
        $("#consultaHome").hide(); 
        $("#number").focus();   
        var t=setInterval("reloj()", 500);	
        var t=setInterval("msj()", 5);
        
        var num = $("#number").val();

        
        //INI IMPRIME CARTOLA
        $('button').attr("disabled",true);
        $('button').click(function(){
            //alert("IMP")
            cartola();
        });
        $('#nombre').css("cursor","pointer");
        $('#nombre').click(function(){
            
            mensaje(400,500,'<?php echo $busqueda?>');
            $('input').on({
                keyup: function(){
                    $(this).css("text-transform","uppercase");
                    },
                keypress: function(){
                    $(this).css("text-transform","uppercase");
                    },
                change: function(){
                    $(this).css("text-transform","uppercase");
                    },
                keydown: function(){
                    $(this).css("text-transform","uppercase");
                    }
                
            });

        });
        
        
        
        if(num!=""){
            traeDatos();
        }
        //FIN IMPRIME CARTOLA
        $('#submit').submit(function() {
            if(valida($("#number").val())==1){
                busqueda();
            }else{
                sms.push("DATO NO VALIDO");
            }
            return false;
        });
        panelControlVisa();
    });
    function msj(){
            
        if(sms.length > 0){
            if(agrupador == docFin){			
                mensaje("a","Alerta!",sms);
                sms = [];
            }
        }
    }
    function busqueda(){
        $('#number').prop('disabled',true);
        var number = $('input[name=number]');

        if($("#adicionalSoloAvance").val()!=""){
            var adicionalActivo = $("#adicionalSoloAvance").val();
            var ArregloAdicionalActivo = adicionalActivo.split("_");
            num = (number.val()==ArregloAdicionalActivo[1])?ArregloAdicionalActivo[0]:number.val();
            var data = 'number=' + num;
        }else{
            var data = 'number=' + number.val();
        }
        //alert(data)
        $.ajax({
                url: "ajax/ajax_consultar.php",
                type: "POST",
                dataType: "json",  
                data: data,
                cache: false,
                //async : false,
                beforeSend: function(){
                    mensaje('1',"Alerta!","<img alt='cargando' title='cargando' src='img/load.gif'>");
                    agrupador++;
                },
                complete: function(){
                    docFin++;
                    $("#mensaje_universal").remove();
                },
                success: function(html) { 
                    $('#consultaSMSSOS').html(html.mensajeSucursalSOS);
                    $('button').attr("disabled",false);
                    $('#number').prop('disabled',false);
                    //alert('entro');
                    if (html.existe == '1') {
                        clasifi();				
                        firma();
                        mensajes(number);			
                        rellena(html, number);
                        panelControlVisa();
                    }
                    if(html.existe=="TQ"){
                        myfunctionTq(number);
                    }
                    if(html.existe=="0"){
                        $("#number2").val(html.existe);
                        sms.push("CLIENTE NO EXISTE");
                    }
                }
            });
    }
    
    function traeDatos(){
        busqueda();		
        return false;
    }
    function rellena(html, number){
        $("#number2").val(html.existe);
        $("#div_cartera").hide();
        
        if(html.arrDatosCliente.subEstado==1){
                $("#tipo_cartera").removeClass("cajainput").removeClass("cajainputRojo");
                $("#div_cartera").show();
                $("#tipo_cartera").html("CASTIGO FINANCIERO");
                $("#tipo_cartera").addClass('cajainputRojo');
                //break;
        }
        
        $("#valoresDisponible").hide();
        $("#nameButton").html('Imprimir Ult. Cartola');
        $("#saldoTag").html('SALDO');
        if(html.arrDatosCliente){
             $("#dg").html(getDV(number.val())); 
             $("#nombre").html(html.arrDatosCliente.nombre+' '+html.arrDatosCliente.aPaterno+' '+html.arrDatosCliente.aMaterno);  
             $("#number").val(html.arrDatosCliente.rut);  
             $("#nombreSucursal").html('('+html.arrDatosCliente.nuSucursal+') '+html.arrDatosCliente.nombreSucursal);  
             $("#direccion").html(html.arrDatosCliente.direccion+' '+html.arrDatosCliente.numeroDir);  
             $("#villa").html(html.arrDatosCliente.villa);  
             $("#idComuna").html(html.arrDatosCliente.comuna);  
             $("#nombreComuna").html('('+html.arrDatosCliente.comuna+') '+html.arrDatosCliente.nombreComuna);  
             $("#telefonoRef").html(html.arrDatosCliente.telefonoRef);  
             $("#totalIngresos").html(fn1 + html.arrDatosCliente.totalIngresos + fn2);
             $("#montoAutorizado").html(fn1 + html.arrDatosCliente.montoAutorizado + fn2);
             $("#CupoDisponible").html(fn1 + html.arrDatosCliente.CupoDisponible + fn2);
             $("#folio").html(html.arrDatosCliente.folio);  
             $("#diaPago").html(html.arrDatosCliente.diaPago);  
             $("#estadoGeneral").val(html.arrDatosCliente.estadoGeneral);  
             $("#fechaIngreso").html(html.arrDatosCliente.fechaIngreso); 
             $("#fechaAprob").html(html.arrDatosCliente.fechaAprovacion);  
             //alert(html.arrDatosCliente.fechaIngreso);
             $("#fechaContrato").html(html.arrDatosCliente.fechaContrato); 
             $("#cobrado").html(fn1 + html.arrDatosCliente.cobrado + fn2);  
            $("#nombreEstadoGeneral").html('('+html.arrDatosCliente.estadoGeneral+') '+html.arrDatosCliente.nombreEstadoGeneral); 
            $("#totalDeuda").html(fn1 + html.sDatosPDC + fn2);
            $("#origenCliente").html('<div><img src="img/'+html.arrDatosCliente.origenEmpresa+'.jpg" alt="Origen Cliente" height="20" width="80"></div>');
        }
        if(html.arrDatosPCV){
             $("#n").html(html.arrDatosPCV.n);
             $("#dias_mora").html(html.arrDatosPCV.dias_mora);
             $("#notifi").html(html.arrDatosPCV.notifi);
             $("#apagardemes").html(fn1 + html.arrDatosPCV.apagardemes + fn2);
             $("#pagos_periodo").html(fn1 + html.arrDatosPCV.pagos_periodo + fn2);
            // $("#pagos_periodo").removeClass("cajainputAmarillo").addClass("cajainput");
             $("#saldo").html(fn1 + html.arrDatosPCV.saldo + fn2);					
             //$("#saldo").removeClass("cajainputAmarillo").addClass("cajainput");
             $("#fecha").html(html.arrDatosPCV.fecha);						 
        }
        if(html.arrDatosPPV){						 
             $("#proxVencimientosFecha1").html(html.arrDatosPPV[0]['proxVencimientosFecha']);			 
             $("#proxVencimientosmonto1").html(fn1 + html.arrDatosPPV[0]['proxVencimientosmonto'] + fn2);							 
             $("#proxVencimientosFecha2").html(html.arrDatosPPV[1]['proxVencimientosFecha']);			 
             $("#proxVencimientosmonto2").html(fn1 + html.arrDatosPPV[1]['proxVencimientosmonto'] + fn2);							 
             $("#proxVencimientosFecha3").html(html.arrDatosPPV[2]['proxVencimientosFecha']);			 
             $("#proxVencimientosmonto3").html(fn1 + html.arrDatosPPV[2]['proxVencimientosmonto'] + fn2);							 
             $("#proxVencimientosFecha4").html(html.arrDatosPPV[3]['proxVencimientosFecha']);			 
             $("#proxVencimientosmonto4").html(fn1 + html.arrDatosPPV[3]['proxVencimientosmonto'] + fn2);							 
             $("#proxVencimientosFecha5").html(html.arrDatosPPV[4]['proxVencimientosFecha']);			 
             $("#proxVencimientosmonto5").html(fn1 + html.arrDatosPPV[4]['proxVencimientosmonto'] + fn2);							 
             $("#proxVencimientosFecha6").html(html.arrDatosPPV[5]['proxVencimientosFecha']);			 
             $("#proxVencimientosmonto6").html(fn1 + html.arrDatosPPV[5]['proxVencimientosmonto'] + fn2);		 
        }
        if(html.arrDatosPUP){
             $("#ultimosPagosFecha1").html(html.arrDatosPUP[0]['ultimosPagosFecha']);			 		 
             $("#ultimosPagosMonto1").html(fn1 + html.arrDatosPUP[0]['ultimosPagosMonto'] + fn2);
             $("#ultimosPagosFecha2").html(html.arrDatosPUP[1]['ultimosPagosFecha']);			 		 
             $("#ultimosPagosMonto2").html(fn1 + html.arrDatosPUP[1]['ultimosPagosMonto'] + fn2);	
             $("#ultimosPagosFecha3").html(html.arrDatosPUP[2]['ultimosPagosFecha']);			 		 
             $("#ultimosPagosMonto3").html(fn1 + html.arrDatosPUP[2]['ultimosPagosMonto'] + fn2);							 
        }	
        if(html.arrjDatosPUIC){
             $("#corres").html(html.arrjDatosPUIC['tipo']);			 		 					 
        }						
        if(html.comentario){
            $("#comentario").html(html.comentario);  
        }
        if(html.rutAdicional!=""){
            $("#adicionalSoloAvance").val(html.rutAdicional);
            $("#tipoCliente").html('ADICIONAL');  
        }else{
            $("#tipoCliente").html('TITULAR');  
        }
        if(html.arrDatosCliente.estadoGeneral==0){
            $("#panelControl").hide();
            //panelControlVisa();
        }else{
            $("#panelControl").show();
        }
           
    }
    function cartola(){
        $('button').attr("disabled",true);
        var valor = $("#number2").val();
        var dato = "";
        if (valor =="TQ"){
            dato = "doc=carvisa";
        }else{
            dato = "doc=cc";
        } 
        
        $.ajax({
                url: "ajax/ajax_impresion.php",
                type: "POST",
                data: dato,
                dataType: "json", 
                cache: false,
                beforeSend: function(){
                    mensaje('1',"Alerta!","Imprimiendo Cartola<br><img alt='cargando' title='cargando' src='img/load.gif'>");
                },
                complete: function(){
                    $("#mensaje_universal").remove();
                },
                success: function(html) {
                    $('button').attr("disabled",false);
                }
            });
    }
    function busquedaApe(pag){
        var materno = $("#materno").val();
        var paterno = $("#paterno").val();
        var data = "materno=" + materno + "&paterno=" + paterno + "&pag=" + pag + "";
        $("#busqueda").html('<img alt="cargando" title="cargando" src="img/load.gif">');
        $.ajax({
                url: "ajax/ajax_consultar_ape.php",
                type: "POST",
                data: data,
                dataType: "json", 
                cache: false,
                success: function(html) {
                    $("#busqueda").html(html)					
                    $('.busquedaid').on({
                        click:function (){
                            $("#number").val($(this).attr("vl"));
                            $('#mensaje_universal').dialog("close");
                            busqueda();
                        },
                        mouseover:function(){
                            $(this).css("border-top","#D6D6C2 3px solid");
                            $(this).css("border-bottom","#D6D6C2 3px solid");
                        },
                        mouseleave:function(){
                            $(this).css("border","white 0px");
                        }
                    });					
                }
            });
    }
    
  </script>
</head>

<body>
<?php include('include/top.php'); ?>

<div class="container1_16 caja16" id="contenedor">
    <div class="caja grid_13 alpha">
        <div class="grid_3" style='font-size:9px'><?php echo $datosEjecutivo['nombre'].' ' .$datosEjecutivo['apePaterno']?></div>
        <div class="grid_3"><b>CONSULTA CLIENTES ! </b></div>
        <div class="grid_1"><?php echo date("d/m/Y") ?></div>
        <div class="grid_2" id="reloj"></div>
        <div class="grid_3"><div id="origenCliente"></div></div>
    </div>
    <div class="clear"></div>
    <div class="grid_12">
        <div id="sms"></div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
    </div>
    <div class="clear"></div>
    <div class="grid_13" style="margin: auto auto 10px auto;">
        <div class="grid_1" >R.U.T:</div>
        <div class="grid_2 cajainput alpha" >
            <form id="submit" action="" autocomplete="on"> 
                <input id="number" name="number"  size=10 maxlength="8" type="text" value="<?php if(isset($_SESSION['datosUsuario']['rut']))echo  $_SESSION['datosUsuario']['rut']; ?>"  />
                <input id="number2" name="number2" size=11 type="hidden" value="0" />
                <input id="estadoGeneral" name="estadoGeneral" size=11 type="hidden" value="0"  />
                <input id="adicionalSoloAvance" name="adicionalSoloAvance" size=11 type="hidden" value="<?php if(isset($_SESSION['datosUsuario']['adicionalActivo']))echo  $_SESSION['datosUsuario']['adicionalActivo']; ?>" />
            </form>
        </div>
        <div class="grid_0-5 cajainput omega" id="dg" >-</div>
        <div class="grid_0-5 link" ><img onclick="javascript:location.href='limpia-datos.php'" src="img/refresh.png" alt="Limpiar" /></div>
        <div class="grid_5 cajainput " id="nombre" >---</div>
        <div class="grid_3 cajainput" id="nombreSucursal">---</div>
    </div>
    <div class="clear"></div>
    
    <div class="grid_12 cajainput" id="comentario">-----------</div>
    
    <div class="clear"></div>
    
    <div class="grid_4">
        <div  >DIRECCION</div> 
        <div class="clear"></div>
        <div class="cajainput " id="direccion">---</div>	
    </div>
    <div class="grid_3-5">
        <div  >VILLA</div>
        <div class="clear"></div>
        <div class="cajainput " id="villa">---</div>
    </div>
    <div class="grid_3" >
        <div>COMUNA</div>
        <div class="clear"></div>
        <div class="cajainput alpha" id="nombreComuna">---</div>	
    </div>
    
    <div class="grid_1-5">
        <div>FONO</div>
        <div class="clear"></div>
        <div class="cajainput" id="telefonoRef">---</div>
    </div>
    <div class="clear"></div>
    <hr>
    <div class="clear"></div>
    <div class="grid_2">
        <div>CORREO</div>
        <div class="clear"></div>
        <div class="cajainput " id="corres">---</div>	
    </div>
    <div class="grid_2">
        <div>CONTRATO</div>
        <div class="clear"></div>
        <div class="cajainput " id="folio">---</div>	
    </div>
    <div class="grid_1">
        <div >TIPO</div>
        <div class="clear"></div>
        <div class="cajainput " id="n">---</div>
    </div>
    <div class="grid_1-5">
        <div>RENTA</div>
        <div class="clear"></div>
        <div class="cajainput omega alpha grid_1-5" id="totalIngresos">---</div>
    </div>
    <div class="grid_1">
        <div class="alpha" >D/PAGO</div>
        <div class="clear"></div>
        <div class="cajainput " id="diaPago">---</div>
    </div>
    <div class="grid_1-5">
        <div class="alpha omega " id="">F/INGRESO</div>
        <div class="clear"></div>
        <div class="alpha omega cajainput" id="fechaIngreso">----</div>
    </div>
    <div class="grid_1-5">
        <div class="alpha omega " id="">F/APROBAC</div>
        <div class="clear"></div>
        <div class="alpha omega cajainput" id="fechaAprob">---</div>
    </div>
    <div class="grid_1-5">
        <div class="alpha omega " id="">FECH/CONTR</div>
        <div class="clear"></div>
        <div class="alpha omega cajainput" id="fechaContrato">---</div>
    </div>
    <div class="clear"></div>	
    
    <div class="clear"></div>
    <div class="grid_2">
        <div>LINEA/CRED</div>
        <div class="clear"></div>
        <div class="grid_2 cajainput  alpha" id="montoAutorizado">---</div>
    </div>
    <div class="grid_1-5">
        <div>DISPONIBLE</div>
        <div class="clear"></div>
        <div class="cajainput grid_1-5 alpha " id="CupoDisponible">---</div>
    </div>
    <div class="grid_1-5">
        <div>TOT/DEUDA</div>
        <div class="clear"></div>
        <div class="cajainput grid_1-5 alpha " id="totalDeuda">---</div>
    </div>
    <!--<div class="grid_1-5">
        <div>COBRADO</div>
        <div class="clear"></div>
        <div class="omega cajainput grid_1-5 alpha" id="cobrado">---</div>	
    </div>-->
    <div class="grid_2-5">
        <div>ESTADO</div>
        <div class="clear"></div>
        <div class="cajainput" id="nombreEstadoGeneral">---</div>
    </div>
    <div class="grid_1-5">
        <div class="alpha omega" >MORA</div>
        <div class="clear"></div>
        <div class="cajainput" id="dias_mora">---</div>
    </div>
    <div class="grid_1-5">
        <div >VCTO ACT</div>
        <div class="clear"></div>
        <div id="fecha" class="cajainput">---</div>
    </div>
    <div class="grid_1-5">
        <div >TIP/CLIENTE</div>
        <div class="clear"></div>
        <div id="tipoCliente" class="cajainput">---</div>
    </div>
    
    <div class="clear"></div>
    <hr > 
    <div class="clear"></div>
    <div class="grid_5 omega">
        <div id="div_cartera" style="display: none;">
            <div class="grid_1-5 alpha omega"><b>CARTERA</b></div>
            <div class="grid_0-5 alpha omega">&nbsp;</div>
            <div id="tipo_cartera" class="grid_3 alpha omega cajainput">-----</div>		
        </div>
        <div class="grid_1-5 alpha omega" ><b>PAGAR MES</b></div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega" ><b>PAGOS/PER</b></div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega"><b><a id="saldoTag">SALDO</a></b></div>	
        <div class="clear"></div>	
        <div class="grid_1-5 alpha omega cajainput">
            <span id="apagardemes">---</span>&nbsp;			
        </div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput " id="pagos_periodo">---</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput " id="saldo">---</div>
        <div class="clear"></div>
        <div style="display:none" id="valoresDisponible">
            <div class="grid_1-5 alpha omega" ><b>DIS.AVANCE</b></div>
            <div class="grid_0-5 alpha omega" >&nbsp;</div>
            <div class="grid_1-5 alpha omega" ><b>DIS.COMPRA</b></div>
            <div class="grid_0-5 alpha omega" >&nbsp;</div>
            <div class="grid_1-5 alpha omega"><b>DIS.COMER</b></div>
            <div class="clear"></div>
            <div class="grid_1-5 alpha omega cajainput" id="dAvance"></div>
            <div class="grid_0-5 alpha omega" >&nbsp;</div>
            <div class="grid_1-5 alpha omega cajainput" id="dCompra"></div>
            <div class="grid_0-5 alpha omega" >&nbsp;</div>
            <div class="grid_1-5 alpha omega cajainput" id="dComer"></div>			
        </div>
        <div class="clear"></div>
        <div class="grid_4 alpha " style="text-align:center;">
            <b>CUOTAS PROXIMOS SEIS MESES</b>
        </div>
        <div class="clear"></div>	
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosFecha1">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosFecha2">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosFecha3">----</div>
        <div class="clear"></div>		
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosmonto1">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosmonto2">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput " id="proxVencimientosmonto3">----</div>
        <div class="clear" style="margin-bottom:5px;"></div>
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosFecha4">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosFecha5">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosFecha6">----</div>
        <div class="clear"></div>	
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosmonto4">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="proxVencimientosmonto5">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput " id="proxVencimientosmonto6">----</div>
        <div class="clear"></div>	
        <div class="grid_4 alpha " style="text-align:center;">
            <b>ULTIMOS TRES PAGOS</b>
        </div>
        <div class="clear"></div> 
        <div class="grid_1-5 alpha omega cajainput" id="ultimosPagosFecha1">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput  " id="ultimosPagosFecha2">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="ultimosPagosFecha3">----</div>
        <div class="clear"></div>
        <div class="grid_1-5 alpha omega cajainput" id="ultimosPagosMonto1">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput  " id="ultimosPagosMonto2">----</div>
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="grid_1-5 alpha omega cajainput" id="ultimosPagosMonto3">----</div>
        <div class="clear"></div>		 	
        <div class="grid_0-5 alpha omega" >&nbsp;</div>
        <div class="clear"></div>
        <div class="grid_4 alpha" style="text-align:center;" id="cambiaBoton">
            <b><button type="text" id="nameButton" >Imprimir Ult. Cartola</button></b>
        </DIV>
        <div class="clear"></div>		
    </div>
    <div class="grid_3 alpha">
        <div class="grid_2-5"><b>&nbsp;&nbsp;&nbsp;CLASIFICACIONES</b></div>
        <div class="clear"></div>
        <div class="grid_2-5"  style="height: 175px;  overflow-y: scroll;" id="clasificacion"></div>
    </div>
    <div class="grid_4-5">
        <div class="clear"></div>
        <b>PANEL DE CONTROL</b>
        <?php 
        if(isset($datosEjecutivo['id_tipo_cuenta']))
            $datosEjecutivoId_tipo_cuenta = $datosEjecutivo['id_tipo_cuenta'];
        else
            $datosEjecutivoId_tipo_cuenta = "";
            
        if(isset($_SESSION["datosSucursal"]["origen"]))
            $datosSucursalorigen = $_SESSION["datosSucursal"]["origen"];
        else
            $datosSucursalorigen = "";
            
        if(isset($_SESSION['datosEjecutivoEvalua']['id_tipo_cuenta']))
            $datosEjecutivoEvaluaid_tipo_cuenta = $_SESSION['datosEjecutivoEvalua']['id_tipo_cuenta'];
        else
            $datosEjecutivoEvaluaid_tipo_cuenta = "";
            
        ?>	
        
        
  <div id="panelControl" class="cajaMenu" >
        <div class="grid_2 alpha omega">
        <?php  foreach($opcMenu as $opc){ ?>
        
    <?php if ($opc['id_menu']==6) { ?> <div id="pncartola"><a onclick="Cartolas()" href="#">CARTOLAS</a><br></div> <?php } ?>
    <?php if ($opc['id_menu']==8) { ?> <div id="pncompra"><a onclick="Compra()" href="#">COMPRA</a><br></div><?php } ?>
    <?php if ($opc['id_menu']==10) { ?> <div id="pnpagos"><a onclick="Pagos()" href="#">PAGOS</a><br></div><?php } ?>
    <?php if ($opc['id_menu']==12) { ?> <div id="pndespago"><a onclick="Despago()" href="#">DESPAGO</a><br></div><?php } ?>
    <?php if ($opc['id_menu']==14) { ?> <div id="pndeuda"><a onclick="Deuda()" href="#">DEUDA</a><br></div><?php } ?>
    <?php if ($opc['id_menu']==16) { ?> <div id="pnseguros"><a onclick="Seguros()" href="#">SEGUROS</a></div><?php } ?>
    <?php if ($opc['id_menu']==18) { ?> <div id="pnhistoricos"> <a onclick="Historicos()" href="#">HISTORICOS</a></div><?php } ?>
    <?php if ($opc['id_menu']==20) { ?> <div id="pnmulticomercio"><a onclick="Multicomercio()" href="#">MULTICOMERCIO</a></div><?php } ?>
    <?php if ($opc['id_menu']==22) { ?> <div id="pnadicional"><a onclick="Adicional()" href="#">ADICIONAL</a> </div><?php } ?>
    <?php if ($opc['id_menu']==24) { ?> <div id="pnhuellamanual"><a onclick="HuellaManual()" href="#">HUELLA MANUAL</a> </div><?php } ?>
    <?php if ($opc['id_menu']==26) { ?> <div id="pncierracuenta"><a onclick="CierraCuenta()" href="#">CIERRE CUENTA</a> </div><?php } ?>
    <?php if ($opc['id_menu']==28) { ?> <div id="pneliminar"><a onclick="Eliminar()" href="#">ELIMINAR</a> </div><?php } ?>
    <?php if ($opc['id_menu']==30) { ?> <div id="pndevolucion"><a onclick="Devolucion()" href="#">DEVOLUCION</a> </div><?php } ?>
    <?php if ($opc['id_menu']==32) { ?> <div style="" id="pncrm"><a onclick="Crm()" href="#">CRM INGRESO</a> </div><?php } ?>
    <?php if ($opc['id_menu']==34) { ?> <div style="" id="pncrmcon"><a onclick="CrmCon()" href="#">CRM CONSULTA</a> </div><?php } ?>
    <?php if($datosSucursalorigen == "aaaaconect"){?>
     <?php if ($opc['id_menu']==36) { ?> <div  id="pnconsultainfo"><a href="#"   onclick="Consultainfo()">INF.COMERCIAL</a> </div><?php } ?>
    <?php } ?>
    <?php if ($opc['id_menu']==38) { ?> <div id="pncrmcon"><a href="#"   onclick="creaUsuarios()">CREA USUARIOS</a></div><?php } ?>
<?php } ?>
    </div>
<div class="grid_2 alpha omega">
<?php  foreach($opcMenu as $opc){ ?>
    <?php if ($opc['id_menu']==7) { ?> <div id="pningreso"><a onclick="Ingreso()" href="#">INGRESO</a> / <a onclick="IngresoCc()" href="#">CC</a> </div><?php } ?>
    <?php if ($opc['id_menu']==9) { ?> <div id="pnavance"><a onclick="Avance()" href="#">AVANCE</a> </div><?php } ?>
    <?php if ($opc['id_menu']==11) { ?> <div id="pnmantencion"><a onclick="Mantencion()" href="#">MANTENCION</a> </div><?php } ?>
    <?php if ($opc['id_menu']==13) { ?> <div id="pnbloqueo"><a onclick="Bloqueo()" href="#">BLOQUEO</a></div><?php } ?>
    <?php if ($opc['id_menu']==15) { ?> <div id="pnsubestado"><a onclick="Subestado()" href="#">DESBLOQUEO</a></div><?php } ?>
    <?php if ($opc['id_menu']==17) { ?> <div id="pnaumentocupo"><a onclick="AumentoCupo()" href="#">AUMENTO CUPO</a></div><?php } ?>
    <?php if ($opc['id_menu']==19) { ?> <div id="pncomentarios"><a onclick="Comentarios()" href="#">COMENTARIOS</a></div><?php } ?>
    <?php if ($opc['id_menu']==21) { ?> <div id="pnrepactacion"><a onclick="Repactacion()" href="#">RENEGOCIACION</a></div><?php } ?>
    <?php if ($opc['id_menu']==23) { ?> <div id="pnpagoscastigo"><a onclick="PagoCastigo()" href="#">PAGO CASTIGO</a> </div><?php } ?>
    <!--<div  <a href="#" onclick="Siniestros()">SINIESTROS</a></div>-->
    <?php if ($opc['id_menu']==25) { ?> <div id="pntarjeta"><a onclick="Tarjeta()" href="#">TARJETA</a></div> <?php } ?>
    <?php if ($opc['id_menu']==27) { ?> <div id="pntarjeta"><a onclick="ClaveFront()" href="#">CLAVE WEB</a></div><?php } ?>
    <?php if ($opc['id_menu']==29) { ?> <div id="pnsernac"><a onclick="Sernac()" href="#">SERNAC</a></div><?php } ?>
    <?php if ($opc['id_menu']==31) { ?> <div id="pnenrolar"><a onclick="Enrolar()" href="#">ENROLAR</a></div><?php } ?>
    <?php if ($opc['id_menu']==33) { ?> <div id="pntraspaso"><a onclick="ImprimirDoc()" href="#">IMPRIMIR</a></div><?php } ?>
    <?php if ($opc['id_menu']==35) { ?> <div id="pntraspaso"><a onclick="Traspaso()" href="#">TRASPASO</a></div><?php } ?>
    <?php if ($opc['id_menu']==37) { ?> <div id="pntraspaso"><a href="#" onclick="Evalua()">EVALUA</a></div><?php } ?>
    <?php if ($opc['id_menu']==39) { ?> <div id="pntraspaso"><a href="#" onclick="Perfiles()">PERFILES</a></div><?php } ?>
        <?php } ?>
        </div>
        
        <div class="clear"></div>
        </div>		
        
        
    </div>
    <div class="clear"></div>
    <div class="grid_4 prefix_8">
        Tricot 2013
    </div>
    
</div>

</body>
</html> 


